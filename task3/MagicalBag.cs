﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    public interface ICreature
    {
        string CreatureType { get; }
    }

    internal class MagicalBag<T> where T : ICreature
    {
        private Dictionary<string, T> gifts;

        public MagicalBag()
        {
            gifts = new Dictionary<string, T>();
        }

        public void AddGift(T gift)
        {
            if (!gifts.ContainsKey(gift.CreatureType))
            {
                gifts.Add(gift.CreatureType, gift);
                Console.WriteLine($"Added a gift for {gift.CreatureType}");
            }
            else
            {
                Console.WriteLine($"A gift for {gift.CreatureType} already exists today");
            }
        }

        public T GetGift(ICreature creature)
        {
            if (gifts.ContainsKey(creature.CreatureType))
            {
                T gift = gifts[creature.CreatureType];
                gifts.Remove(creature.CreatureType);
                Console.WriteLine($"Got a gift for {creature.CreatureType}");
                return gift;
            }
            else
            {
                Console.WriteLine($"No gift available for {creature.CreatureType} today");
                return default(T);
            }
        }
    }
}
