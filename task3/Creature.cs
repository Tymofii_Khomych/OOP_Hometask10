﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Creature : ICreature
    {
        public string CreatureType { get; }

        public Creature(string creatureType)
        {
            CreatureType = creatureType;
        }

    }
}
