﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MagicalBag<ICreature> bag = new MagicalBag<ICreature>();

            var creature1 = new Creature("Goblin");
            var creature2 = new Creature("Elf");
            var creature3 = new Creature("Goblin");

            bag.AddGift(creature1);
            bag.AddGift(creature2);
            bag.AddGift(creature3);

            var gift1 = bag.GetGift(creature1); 
            var gift2 = bag.GetGift(creature2); 
            var gift3 = bag.GetGift(creature3); 
        }
    }
}
